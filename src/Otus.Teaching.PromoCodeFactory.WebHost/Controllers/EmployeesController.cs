﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="employeeCreateCommand">данные сотрудника</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Guid> CreateEmployee(EmployeeCreateCommand employeeCreateCommand)
        {
            var employee = new Employee
            {              
                FirstName = employeeCreateCommand.FirstName,
                LastName = employeeCreateCommand.LastName,
                Email = employeeCreateCommand.Email,
                AppliedPromocodesCount = employeeCreateCommand.AppliedPromocodesCount
            };
            return await _employeeRepository.Add(employee);
        }

        /// <summary>
        /// Редактировать сотрудника
        /// </summary>
        /// <param name="id">Id сотрудника</param>
        /// <param name="employeeUpdateCommand">данные сотрудника</param>
        /// <returns></returns>
        [HttpPut("{id:guid}", Name ="UpdateEmployeeById")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<bool>> UpdateEmployee(Guid id, [FromBody] EmployeeUpdateCommand employeeUpdateCommand)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var existedEmployee = await _employeeRepository.GetByIdAsync(id);
            if (existedEmployee == null)
                return NotFound("Не найден сотрудник с Id = " + id + " в БД!");

            existedEmployee.FirstName = employeeUpdateCommand.FirstName;
            existedEmployee.LastName = employeeUpdateCommand.LastName;
            existedEmployee.Email = employeeUpdateCommand.Email;
            existedEmployee.AppliedPromocodesCount = employeeUpdateCommand.AppliedPromocodesCount;
            
            await _employeeRepository.Update(existedEmployee);
            return true;
        }

        /// <summary>
        /// Грохнуть сотрудника
        /// </summary>
        /// <param name="id">ID сотрудника</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}", Name ="DeleteEmployeeById")]
        public async Task<ActionResult<bool>> DeleteEmployee(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var existedEmployee = await _employeeRepository.GetByIdAsync(id);
            if (existedEmployee == null)
                return NotFound("Не найден сотрудник с Id = " + id + " в БД!");

            await _employeeRepository.Delete(existedEmployee);
            
            return true;
        }
    }
}