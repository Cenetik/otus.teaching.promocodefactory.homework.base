﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public record EmployeeCreateCommand(string FirstName, string LastName, string Email, int AppliedPromocodesCount);
}
