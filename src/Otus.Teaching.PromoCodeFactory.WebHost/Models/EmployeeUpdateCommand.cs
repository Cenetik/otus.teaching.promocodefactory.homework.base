﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public record EmployeeUpdateCommand(string FirstName, string LastName, string Email, int AppliedPromocodesCount);
}
