﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task<Guid> Add(T entry)
        {
            entry.Id = Guid.NewGuid();
            await Task.FromResult(Data = Data.Append(entry).ToList());            
            return entry.Id;
        }

        public Task Update(T entry)
        {
            var existed = Data.FirstOrDefault(p=>p.Id == entry.Id);
            if (existed == null)
                throw new Exception("Не найден объект для изменения в БД с Id = " + entry.Id + "!");
            Data = Data.Where(p=>p.Id != entry.Id).ToList();
            Data = Data.Append(entry).ToList();
            return Task.FromResult(Data);
        }

        public Task Delete(T entry)
        {
            Data = Data.Where(p => p.Id != entry.Id).ToList();
            return Task.FromResult(Data);
        }
    }
}